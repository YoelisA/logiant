# logiAnt

Une implémentation en Elm de la fourmi de Langton (Langton Ant). Un automate cellulaire obéissant à des règles très simples : 

- Une **fourmi** se déplace en avant sur un **plan** constitué de **cellules** **blanches** et **noires**.
- Cette fourmi tourne à droite lorsqu'elle arrive sur une cellule blanche, et à gauche lorsqu'elle arrive une cellule noire. 
- Lorsque la fourmi quitte la cellule, la couleur de cette dernière est inversée.

## Pour faire tourner ce programme sur votre machine

```bash
git clone https://gitlab.com/YoelisA/logiant.git
cd logiant
npm install
npm run start
```

[Cliquez-ici pour accéder à la démo](https://logiant.netlify.app/)

## Note d'intention

J'ai choisi de réaliser ce kata parce qu'il offre plusieurs intérêts. Je suis très curieux des processus émergents et je n'ai jamais implémenté ce type d'automate.

- Comment tester ce programme ? Test unitaires ? Fuzzing ?
- Comment représenter de façon idiomatique ce problème ? 
  - Pour le plan, un array à deux dimensions ? 
  - Un dictionnaire avec des coordonnées de la cellule en clé ? 
  - Des zippers emboités ?

### Pourquoi Elm ? 

Elm a un système de type éloquent permettant de représenter de façon élégante ce type de problèmes. C'est aussi le langage avec lequel je suis le plus à l'aise et c'est une bonne façon de le faire connaître!

### Choses à faire

- - [x]  Bootstrapper le projet
    - [x] gitlab
    - [x] avec snowpack pour tester?
    - [x] Ajouter les instructions pour faire tourner le programme en local.
    - [x] Déployer la démo sur netlify.
  
- [x] Types et règles
- [x] Générer une grille aléatoire
- [x] Afficher la grille 
- [ ] Générer aléatoirement la position de la fourmi
- [x] Afficher la fourmi
  - [x] Polygon svg
  - [x] Rotation en fonction de la direction
- [x] Ajouter les ticks et déplacer la fourmi dans la grille
- [ ] Reste à faire
  - [ ] Les fuzz tests pour générer différentes grilles, positions de fourmi mais comment générer les résultats attendus ?
  - [ ] L'UI pour modifier les paramètres (fréquence du tick, taille de la grille, nombre de cases noires) 
  - [ ] Arrêter la simulation quand la fourmi est hors du plan?

## Commentaires et réflexions 

J'ai passé beaucoup trop de temps à bootstrapper le projet. Cela m'a laissé peu de temps pour coder à proprement parlé et je n'ai pas pu tester les deux méthodes que je voyais initialement (le dictionnaire vs les zippers). Je me suis également précipité sur l'implémentation en mettant de côté les tests, ce qui m'a conduit à bloquer sur un bug parce que les règles étaient mal définies. J'espère avoir un peu de temps pour cette semaine pour améliorer un peu le rendu final et ajouter quelques paramètres modifiables par l'utilisateur! Je voudrais aussi ajouter un fuzzer pour tester l'automate sans avoir à passer par les tests unitaires de toutes les règles. 