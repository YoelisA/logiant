module Main exposing (..)

import Browser
import Color
import Dict exposing (Dict)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Input exposing (button, defaultThumb, labelAbove)
import Html.Attributes as HA
import List.Extra exposing (lift2)
import Random
import Time
import TypedSvg as Svg
import TypedSvg.Attributes exposing (id, points, transform, x, y)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (Paint(..), percent)



-- https://samtay.github.io/posts/comonadic-game-of-life.html pour avoir une comparaison entre Plane (Zipper Comonades) et Plane (Hashmap)


cellSize =
    20


planeInitialSize =
    20


type alias Cell =
    { position : Coordinates
    , color : Color
    }


type alias Ant =
    { position : Coordinates
    , direction : Direction
    }


type alias Dimension =
    ( Int, Int )


type alias Plane =
    Dict Coordinates Cell


type Color
    = Black
    | White


type alias Coordinates =
    ( Int, Int )


type Direction
    = North
    | West
    | East
    | South



-- MODEL


type alias Model =
    { plane : Plane, ant : Ant, antSpeed : Float }


init : () -> ( Model, Cmd Msg )
init _ =
    ( { plane = initPlane
      , ant = initAnt
      , antSpeed = 100.0
      }
    , generatePlane
    )


generatePlane =
    Random.generate RuntimeGeneratedPlane
        (planeGenerator
            ( 30
            , 30
            )
        )


initPlane : Plane
initPlane =
    Dict.empty


initAnt =
    Ant ( 20, 20 ) North


planeGenerator : Dimension -> Random.Generator Plane
planeGenerator ( w, h ) =
    let
        rows =
            List.range 0 w

        cols =
            List.range 0 h

        allCoordinates =
            lift2 (\x_ y_ -> ( ( x_, y_ ), Cell ( x_, y_ ) White )) rows cols |> Dict.fromList

        planeWithBlackCells =
            Random.int 0 (w * h // 10)
                |> Random.andThen
                    (\range ->
                        Random.list range (Random.pair (Random.int 0 w) (Random.int 0 h))
                    )
                |> Random.andThen
                    (\coordinates ->
                        Random.constant <|
                            List.foldr
                                (\coordinate acc ->
                                    Dict.insert coordinate (Cell coordinate Black) acc
                                )
                                allCoordinates
                                coordinates
                    )
    in
    planeWithBlackCells



-- UPDATE


type Msg
    = RuntimeGeneratedPlane Plane
    | Tick Time.Posix
    | UserPressedRerunSimulation
    | UserChangedAntSpeed Float


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RuntimeGeneratedPlane plane ->
            ( { model | plane = plane }, Cmd.none )

        UserChangedAntSpeed newSpeed ->
            ( { model | antSpeed = newSpeed }, Cmd.none )

        UserPressedRerunSimulation ->
            ( { model | plane = initPlane, ant = initAnt }, generatePlane )

        Tick timestamp ->
            let
                cell : Cell
                cell =
                    Dict.get model.ant.position model.plane
                        |> Maybe.withDefault (Cell ( 0, 0 ) White)

                -- Faudrait ajouter une fin de la simulation et une erreur
                updatedCell =
                    switchColor cell

                updatedPlane =
                    Dict.insert updatedCell.position updatedCell model.plane

                updatedAnt =
                    move model.ant cell
            in
            ( { model
                | ant = updatedAnt
                , plane = updatedPlane
              }
            , Cmd.none
            )


{-| Ant's legal moves. Todo : write fuzz test to check for more situations.
-}
move : Ant -> Cell -> Ant
move ant cell =
    case ( cell.color, ant.direction ) of
        ( White, North ) ->
            Ant (toEast ant.position) East

        ( Black, North ) ->
            Ant (toWest ant.position) West

        ( White, East ) ->
            Ant (toSouth ant.position) South

        ( Black, East ) ->
            Ant (toNorth ant.position) North

        ( White, South ) ->
            Ant (toWest ant.position) West

        ( Black, South ) ->
            Ant (toEast ant.position) East

        ( White, West ) ->
            Ant (toNorth ant.position) North

        ( Black, West ) ->
            Ant (toSouth ant.position) South


switchColor : Cell -> Cell
switchColor cell =
    case cell.color of
        Black ->
            Cell cell.position White

        White ->
            Cell cell.position Black


toNorth : Coordinates -> Coordinates
toNorth position =
    Tuple.mapSecond (\pos -> pos - 1) position


toSouth : Coordinates -> Coordinates
toSouth position =
    Tuple.mapSecond (\pos -> pos + 1) position


toWest : Coordinates -> Coordinates
toWest position =
    Tuple.mapFirst (\pos -> pos - 1) position


toEast : Coordinates -> Coordinates
toEast position =
    Tuple.mapFirst (\pos -> pos + 1) position



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every model.antSpeed Tick



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = "Logiant"
    , body =
        [ layout [ width fill, height fill ] (body model) ]
    }


grey =
    rgb255 211 214 217


body : Model -> Element Msg
body model =
    row [ width fill, height fill ]
        [ column [ width fill, height fill ] [ text "Left pan" ]
        , column [ centerX, centerY, Background.color (rgb255 211 214 217), height fill ]
            [ html <|
                Svg.svg [ TypedSvg.Attributes.viewBox 0 0 800 800, TypedSvg.Attributes.width (percent 100) ] <|
                    [ renderPlane model.ant model.plane ]
            ]
        , column [ width fill, height fill ]
            [ text "options"
            , button []
                { onPress = Just UserPressedRerunSimulation
                , label = el [] (text "Rerun simulation")
                }
            , Element.Input.slider
                [ htmlAttribute (HA.style "transform" "rotateY(180deg)")
                , Element.behindContent
                    (Element.el
                        [ Element.width Element.fill
                        , Element.height (Element.px 2)
                        , Element.centerY
                        , Background.color grey
                        , Border.rounded 2
                        ]
                        Element.none
                    )
                , width (fill |> maximum 300)
                ]
                { onChange = UserChangedAntSpeed
                , label = labelAbove [] (text "Change ant's speed")
                , min = 5.0
                , max = 200.0
                , value = model.antSpeed
                , thumb = defaultThumb
                , step = Just 5.0
                }
            ]
        ]


renderPlane : Ant -> Plane -> Svg msg
renderPlane ant plane =
    let
        instantiateAnt : Svg msg
        instantiateAnt =
            renderAnt ant

        flip =
            \a b c -> a c b
    in
    Dict.toList plane
        |> List.map (\( _, cell ) -> renderCell cell)
        |> flip List.append [ instantiateAnt ]
        |> Svg.g []


renderCell : Cell -> Svg msg
renderCell cell =
    Svg.rect
        [ TypedSvg.Attributes.width (TypedSvg.Types.px cellSize)
        , TypedSvg.Attributes.height (TypedSvg.Types.px cellSize)
        , colorGivenCellColor cell
        , x (TypedSvg.Types.px <| toFloat <| Tuple.first cell.position * cellSize)
        , y (TypedSvg.Types.px <| toFloat <| Tuple.second cell.position * cellSize)
        , id <| "cell:" ++ String.fromInt (Tuple.first cell.position) ++ String.fromInt (Tuple.second cell.position)
        ]
        []


colorGivenCellColor cell =
    if cell.color == White then
        TypedSvg.Attributes.fill (Paint Color.gray)

    else
        TypedSvg.Attributes.fill (Paint Color.black)


renderAnt : Ant -> Svg msg
renderAnt ant =
    let
        ant_ =
            Svg.polygon
                [ points [ ( 18, 15 ), ( 12, 9 ), ( 6, 15 ) ]
                , TypedSvg.Attributes.fill (Paint Color.red)
                , id "ant"
                , transform
                    [ TypedSvg.Types.Translate (toFloat <| Tuple.first ant.position * cellSize) (toFloat <| Tuple.second ant.position * cellSize)
                    , TypedSvg.Types.Rotate (rotationGivenDirection ant.direction) (toFloat cellSize / 2) (toFloat cellSize / 2)
                    ]
                ]
                []
    in
    ant_


rotationGivenDirection : Direction -> Float
rotationGivenDirection direction =
    case direction of
        North ->
            0

        West ->
            -90

        East ->
            90

        South ->
            180



-- MAIN


main =
    Browser.document
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }
