module TestSuite exposing (..)

import Expect
import Main
import Test exposing (..)


suite : Test
suite =
    describe "Ant legal moves"
        [ test "Looking at North, ant moves East when on a white cell" <|
            \_ ->
                let
                    cell =
                        Main.Cell ( 0, 0 ) Main.White

                    ant =
                        Main.Ant ( 0, 0 ) Main.North
                in
                Expect.equal (Main.Ant ( 0, 1 ) Main.East) (Main.move ant cell)
        , test "Looking at South, ant moves East when on a black cell" <|
            \_ ->
                let
                    cell =
                        Main.Cell ( 0, 0 ) Main.Black

                    ant =
                        Main.Ant ( 0, 0 ) Main.South

                    newAnt =
                        Main.Ant ( 0, 1 ) Main.West
                in
                Expect.equal newAnt (Main.move ant cell)
        ]
